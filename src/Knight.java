public class Knight extends Fighter {

	public Knight(int baseHp, int wp) {
		super(baseHp, wp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getCombatScore() {
		if(Utility.isSquare(Battle.GROUND)==false)
      { 
		if(getWp()==1)
		{
			return (double)getBaseHp();
		}
		else 
		{	
			return (double)getBaseHp()/10;
		}
	  }
		else {
			return (double)getBaseHp()*2;
		}
	}
   /* double combat;
    * if (super.getWp() == 1) combat  = super.getBaseHp();
    * else combat = super.getBaseHp() /10;
    * if (Utility.isSquare(Battle.ground)) combat = super.getBaseHp()*2;
    * return combat;
    */
}
