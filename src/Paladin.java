
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);
		// TODO Auto-generated constructor stub
	}
	static boolean isPerfectSquare(int x)
	{
		int s= (int)Math.sqrt(x);
		return (s*s==x);
	}
	static boolean isFibonacci(int n)
	{
		return isPerfectSquare(5*n*n+4)|| isPerfectSquare(5*n*n-4); 
	}
	int findIndex(int n)
	{
		if(n<=1)
		{
			return n;
		}
		int a=0 , b=1 , c=1 ;
		int res=1;
		while(c<n)
		{
			c= a+b;
			res++;
			a=b;
			b=c;
		}
		return res;
	}
    
	public double getCombatsocre()
	{
			if(isFibonacci(this.getBaseHp()))
			{
				if(findIndex(this.getBaseHp())>2)
				{
					return (1000+ findIndex(this.getBaseHp()));
				}
				else
				{
					return this.getBaseHp()*3;
				}
			}
		else
		{
			return this.getBaseHp()*3;
		}
	}
	
}
