public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getCombatScore() {
		if(Utility.isSquare(Battle.GROUND)==false)
		{
			if(getWp()==1)
			{
				return (double)getBaseHp();
			}
			else
			{
				return (double)getBaseHp()/10;
			}
		}
		else 
		{
			return (double)getBaseHp()*2;
		}
	}
    
}
